{-#LANGUAGE RecordWildCards#-}
{-#LANGUAGE ScopedTypeVariables#-}
module Libp2p.Mux.Const (
    initialStreamWindow,
    Header(..),
    MsgType(..),
    protoVersion,
    flagSYN,
    flagACK,
    flagFIN,
    flagRST,
    mkHeader,
    headerSize,
    intToMsgType,
    goAwayNormal,
    goAwayProtoErr,
    goAwayInternalErr
) where
import Data.Serialize
import Data.Word
import Data.Bits

initialStreamWindow::Int
initialStreamWindow = 256 * 1024

protoVersion = 0::Word8

flagSYN = 1::Word16
flagACK = 1 `shift` 1::Word16
flagFIN = 1 `shift` 2::Word16
flagRST = 1 `shift` 3::Word16

sizeOfVersion = 1::Int
sizeOfType = 1::Int
sizeOfFlags = 2::Int
sizeOfStreamID = 4::Int
sizeOfLength = 4::Int

goAwayNormal = 0::Int
goAwayProtoErr = 1::Int
goAwayInternalErr = 2::Int

headerSize::Int
headerSize = sizeOfVersion + sizeOfType + sizeOfFlags + sizeOfStreamID + sizeOfLength

data MsgType = TypeData | TypeWindowUpdate | TypePing | TypeGoAway deriving (Eq,Show)


intToMsgType::Int -> Maybe MsgType
intToMsgType 0 = Just TypeData
intToMsgType 1 = Just TypeWindowUpdate
intToMsgType 2 = Just TypePing
intToMsgType 3 = Just TypeGoAway
intToMsgType _ = Nothing

msgTypeToInt::MsgType -> Int
msgTypeToInt TypeData = 0
msgTypeToInt TypeWindowUpdate = 1
msgTypeToInt TypePing = 2
msgTypeToInt TypeGoAway = 3

data Header = Header {
    headerVersion::Word8,
    headerMsgType::MsgType,
    headerFlags::Word16,
    headerStreamId::Word32,
    headerLength::Word32
} deriving (Eq,Show)

mkHeader::MsgType -> Word16 -> Word32 -> Word32 -> Header
mkHeader msgType flags streamId length = Header {
  headerVersion = protoVersion,
  headerMsgType = msgType,
  headerFlags = flags,
  headerStreamId = streamId,
  headerLength = length
}

instance Serialize Header where
  put Header{..} = do
    putWord8 headerVersion
    putWord8 (fromIntegral $ msgTypeToInt headerMsgType)
    putWord16be headerFlags
    putWord32be headerStreamId
    putWord32be headerLength
  get = do
    headerVersion     <- getWord8
    headerMsgTypeNum  <- getWord8
    let mayMsgType::Maybe MsgType = intToMsgType $ fromIntegral headerMsgTypeNum
    case mayMsgType of
      Nothing -> fail $ "msgtype error " <> show headerMsgTypeNum <> "\r\n"
      Just  headerMsgType  -> do
                   headerFlags    <- getWord16be
                   headerStreamId <- getWord32be
                   headerLength   <- getWord32be
                   return Header{..}